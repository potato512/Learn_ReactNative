import React from 'react';
import {View, Button, Animated, LayoutAnimation} from 'react-native';

export default class AnimationPage extends React.Component {
  state = {
    widthView:30,
    heightView:30,

    fadeAnim: new Animated.Value(0), // 透明度初始值设为0
  };

  render(){
    return (
      <View style={{marginTop:60,alignItems:"center"}}>
        <Button title="变大" onPress={() => {
          LayoutAnimation.spring();
          this.setState({
            widthView:this.state.widthView + 5,
            heightView:this.state.heightView + 5,
          })
        }}></Button>
        <Button title="变小" onPress={() => {
          LayoutAnimation.spring();
          this.setState({
            widthView:this.state.widthView - 5,
            heightView:this.state.heightView - 5,
          })
        }}></Button>
        <View style={[{backgroundColor:"#00FA9A", width:30,height:30},{width:this.state.widthView,height:this.state.heightView}]}></View>

        <Button title="变亮" onPress={() => {
          Animated.timing(
            this.state.fadeAnim,
            {toValue: 1,}
          ).start();
          }
        }></Button>
        <Animated.view style={{opacity: this.state.fadeAnim}}>
          <View style={{backgroundColor:"red",width:50,height:50}}></View>
          {this.props.children}
        </Animated.view>

      </View>
    );
  }
};
