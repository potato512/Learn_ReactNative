import React from 'react';
import {View, Text, Alert, StyleSheet} from 'react-native';


export default class LifeCyclePage extends React.Component {

  /*
  一、初始化及挂载阶段
  1、构造函数：constructor(props) { super(props); this.state = { //key : value }; }
  2、组件加载到DOM：componentWillMount() { }
  3、
  二、支行阶段

  三、卸载阶段

  */


  /**
  初始化及加载阶段
  1 组件类的构造函数
  通常在此初始化state数据模型
  */
  constructor(props) {
    super(props);
    this.state = {
      //key : value
      message:""
    };
  }

  /**
  初始化及加载阶段
  2 组件将要加载到虚拟DOM
  在render方法之前执行
  整个生命周期只执行一次
  */
  componentWillMount() {
    // Alert.alert("componentWillMount")
    console.log("componentWillMount");
  }

  /**
  初始化及加载阶段 运行阶段
  render方法用于渲染组件
  在初始化阶段和运行期阶段都会执行
  */
  render() {
    return(
      <View onPress={tapClick()}>
        <Text>生命周期</Text>
      </View>
    )
  }

  /**
  初始化及加载阶段
  4 组件已经加载到虚拟DOM
  在render方法之后执行
  整个生命周期只执行一次
  通常在该方法中完成异步网络请求或者集成其他JavaScript库
  */
  componentDidMount() {
    // Alert.alert("componentDidMount")
    console.log("componentDidMount");
  }

  /**
  运行阶段
  在组件接收到其父组件传递的props的时候执行，参数为父组件传递的props
  在组件的整个生命周期可以多次执行
  通常在此方法接收新的props值，重新设置state
  */
  componentWillReceiveProps(nextProps) {
    this.setState({
      //key : value
      message:"接收父类组件值"
    });
    // Alert.alert("componentWillReceiveProps")
    console.log("componentWillReceiveProps");
  }

  /**
  运行阶段
  在componentWillReceiveProps(nextProps)执行之后立刻执行
  或者在state更改之后立刻执行
  该方法包含两个参数，分别是props和state
  该方法在组件的整个生命周期可以多次执行
  如果该方法返回false，则componentWillUpdate(nextProps, nextState)及其之后执行的方法都不会执行，组件则不会进行重新渲染
  */
  shouldComponentUpdate(nextProps, nextState) {
    // Alert.alert("shouldComponentUpdate")
    console.log("shouldComponentUpdate");
    return true;
  }

  /**
  运行阶段
  在shouldComponentUpdate(nextProps, nextState)函数执行完毕之后立刻调用
  该方法包含两个参数，分别是props和state
  render()函数执行之前调用
  该方法在组件的整个生命周期可以多次执行
  */
  componentWillUpdate(nextProps, nextState) {
    // Alert.alert("componentWillUpdate")
    console.log("componentWillUpdate");
  }

  /**
  运行阶段
  在render()方法执行之后立刻调用
  该方法包含两个参数，分别是props和state
  该方法在组件的整个生命周期可以多次执行
  */
  componentDidUpdate(preProps, preState) {
    // Alert.alert("componentDidUpdate")
    console.log("componentDidUpdate");
  }

  /**
  卸载阶段
  在组件由虚拟DOM卸载的时候调用
  */
  componentWillUnmount() {
    // Alert.alert("componentWillUnmount")
    console.log("componentWillUnmount");
  }
}

const tapClick = () => {
  // Alert.alert("render")
  console.log("render");
}
