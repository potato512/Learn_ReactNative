/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

// 1 导入开发需要的类，或组件
import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
} from 'react-native';
import { createStackNavigator } from 'react-navigation';



import ImagePage from './Components/ViewPage/ImagePage';
import ButtonPage from './Components/UserInterfacePage/ButtonPage';
import FlastListPage from "./Components/ListPage/FlastListPage";
import SectionListPage from "./Components/ListPage/SectionListPage";
import TextPage from "./Components/ViewPage/TextPage";
import StyleSheetPage from "./Components/ViewPage/StyleSheetPage";
import TextInputPage from "./Components/ViewPage/TextInputPage";
import TouchableOpacityPage from "./Components/UserInterfacePage/TouchableOpacityPage";
import TouchableHighlightPage from "./Components/UserInterfacePage/TouchableHighlightPage";
import TouchableNativeFeedbackPage from "./Components/UserInterfacePage/TouchableNativeFeedbackPage";
import ScrollViewPage from "./Components/ViewPage/ScrollViewPage";
import WebViewPage from "./Components/OtherPage/WebViewPage";
import DataStoragePage from "./Components/DataStorage/DataStoragePage";
import LifeCyclePage from "./LifeCycle/LifeCyclePage";
import NavigationPage from "./Components/OtherPage/NavigationPage";
import NavigationPageNext from './Components/OtherPage/NavigationPageNext';
import NavigationPageDetail from './Components/OtherPage/NavigationPageDetail';
import AlertPage from "./Components/OtherPage/AlertPage";
import NetworkRequest from './NetworkRequest/NetworkRequest';
import AnimationPage from './Animation/AnimationPage';

const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' +
    'Cmd+D or shake for dev menu',
  android: 'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});

// 方法1
// const RootStack = createStackNavigator({
//   Home: {
//     screen: NavigationPage
//   },
// });
// 方法2（用此方法）
const RootStack = createStackNavigator(
  {
    // 定义路由页面
    Home: NavigationPage,
    homeNext: NavigationPageNext,
    Details: NavigationPageDetail,
  },
  {
    // 初始化首页
    initialRouteName: 'Home',
  },
);



// 2 定义组件
type Props = {};
export default class App extends Component<Props> {
  render() {
    return (
      // <NavigationPage></NavigationPage>
      <AnimationPage />
    );
    // return <RootStack />;
  }
}

// 3 创建样式实例，只实例化一次
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    backgroundColor: 'yellow',

    fontSize: 20,
    color: 'red',
    textAlign: 'center',
    marginTop: 20,
    marginHorizontal: 10,
  },
  instructions: {
    borderRadius: 10,
    borderStyle: 'dashed',
    borderColor: 'green',
    borderWidth: 1,

    width: 300,

    shadowColor: 'red',

    // lineHeight: 50,
    textAlign: 'center',
    color: '#333333',
    marginTop:20,
    marginHorizontal:10,
  },
});
