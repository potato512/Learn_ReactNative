import React from 'react'
import {View, Text, TouchableOpacity, Button, Image} from 'react-native'
// import { createStackNavigator } from 'react-navigation';
// import CardStackStyleInterpolator from 'react-navigation/src/views/CardStack/CardStackStyleInterpolator';

export default class NavigationPageNext extends React.Component {

  constructor(props){
        super(props);
        _this = this;
    }

  // 配置导航栏
  static navigationOptions = {
    // 导航栏标题，推荐使用
    headerTitle: (
      <Button title="homeNext"
      color="black"
      onPress={() => alert("点击民航栏标题")}>
      </Button>
    ),
    // 导航栏返回按钮标题
    headerBackTitle:"返回",
    // 导航条左侧，可以是按钮或者其他视图控件
    headerLeft: (
      <TouchableOpacity onPress={() =>{
          _this.props.navigation.goBack();
      }}>
        <Image style={{marginLeft:20, width:30, height:30}} source={require("../../Resources/leftBack.png")} resizeMode='center'></Image>
      </TouchableOpacity>
    ),
    // 导航条右侧。可以是按钮或者其他视图控件
    headerRight: (
      <Button title="Info"
      color="red"
      onPress={() => {
        _this.props.navigation.navigate('Details')
      }}/>
    ),
  };

  render() {
    return(
      <View style={{marginTop:60,alignItems:"center"}}>
        <Text>导航栏使用: {this.props.navigation.state.params.message}</Text>
        <Button title="navigate to homeNext" onPress={() => {
          // navigae过滤现有导航，即如果已导航到视图B，再导航时不会导航
          this.props.navigation.navigate('homeNext')
        }} />
        <Button title="push to homeNext" onPress={() => {
          // push不过滤现有导航，即如果已导航到视图B，再导航时继续导航
          this.props.navigation.push('homeNext', {message:"导航过来了"})
        }} />
        <Button title="navigate to Details" onPress={() => {
          this.props.navigation.navigate('Details')
        }} />
        <Button title="navigate goBack" onPress={() => {
          this.props.navigation.goBack()
        }} />

      </View>
    )
  }
}
