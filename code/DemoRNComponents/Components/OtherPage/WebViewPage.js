import React from 'react';
import {View, Text, WebView, Dimensions, StyleSheet} from 'react-native';

// 获取屏幕的宽度和高度
let widthScreen = Dimensions.get('window').width;
let heightScreen = Dimensions.get('window').height;
let url = "https://www.toutiao.com";

export default class WebViewPage extends React.Component {
    render() {
      return(
        <View style={styles.container}>
          <Text style={styles.text}>WebView视图</Text>
          <WebView
            style={styles.webView}
            source={{uri: url, method: 'GET'}}
            startInLoadingState={true}
            scrollEnabled={true}
            scalesPageToFit={true}
            javaScriptEnabled={true}
            mediaPlaybackRequiresUserAction={true}
            allowsInlineMediaPlayback={true}
            domStorageEnabled={false}
            bounces={true}/>
        </View>
      )
    }
}

var styles = StyleSheet.create({
  container: {
    display:"flex",
    flexDirection:"column",
    flex: 1,
    backgroundColor: "white",
  },
  text:{
    textAlign:"center",
    color:"black",
    width:"100%",
    height:40,
  },
  webView: {
    backgroundColor: "#F0F0F0",
    width:"100%",
    height: (heightScreen - 40),
  },
});
