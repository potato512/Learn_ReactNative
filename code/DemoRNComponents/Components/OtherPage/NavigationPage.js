import React from 'react'
import {View, Text, Button} from 'react-native'
// import { createStackNavigator } from 'react-navigation';

export default class NavigationPage extends React.Component {

  // 配置导航栏
  static navigationOptions = {
    // 导航栏标题，不推荐
    title: 'Home', 
    // 设置导航栏颜色
    headerTintColor: "green",
    // 导航条的样式，如背景色，宽高等
    headerStyle:{
        backgroundColor:"#1E90FF",
    },
    // 导航栏文字样式
    headerTitleStyle:{
        paddingLeft:20,
        fontSize:30
    },
  };


  render() {
    return(
      <View style={{marginTop:60,alignItems:"center"}}>
        <Text>导航栏使用</Text>
        <Button title="navigate to homeNext" onPress={() => {
          this.props.navigation.navigate('homeNext', {message:"导航过来了"})
        }} />
      </View>
    )
  }
}


// 方法1
// class NavigationPage extends React.Component {
//   render() {
//     return(
//       <View style={{marginTop:60,alignItems:"center"}}>
//         <Text>导航栏使用</Text>
//       </View>
//     )
//   }
// }
// export default createStackNavigator({
//   Home: {
//     screen: NavigationPage
//   },
// });
