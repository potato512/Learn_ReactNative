import React from 'react'
import {View, Text, Button} from 'react-native'
// import { createStackNavigator } from 'react-navigation';

export default class NavigationPageDetail extends React.Component {

  // 配置导航栏
  static navigationOptions = {
    header:null // 全屏，即隐藏导航栏
  };

  render() {
    return(
      <View style={{marginTop:60,alignItems:"center"}}>
        <Text>导航栏使用</Text>
        <Button title="Go to Details" onPress={() => {
          this.props.navigation.navigate('Details')
        }} />
        <Button title="navigate pop" onPress={() => {
          this.props.navigation.pop()
        }} />
        <Button title="navigate goBack" onPress={() => {
          this.props.navigation.goBack()
        }} />
        <Button title="navigate popToTop" onPress={() => {
          this.props.navigation.popToTop()
        }} />

      </View>
    )
  }
}
