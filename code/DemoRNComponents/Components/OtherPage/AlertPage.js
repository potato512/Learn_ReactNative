import React from 'react';
import {View, Text, Button, Alert, AlertIOS} from "react-native";

const title = "温馨提示";
const message = '要下雨了，记得带伞'

export default class AlertPage extends React.Component {

  constructor(props) {
      super(props);
      _this = this;
      this.state = {
          inputText:'',
      };
  }

   render(){
     return(
       <View style={{marginTop:60, alignItems:"center"}}>
         <Text>Alert的使用</Text>
         <Button title="标题+确定" onPress={() => {
           Alert.alert(title)
         }}/>
         <Button title="标题+内容+确定" onPress={() => {
           Alert.alert(title,message)
         }}/>
         <Button title="标题+内容+取消+确定" onPress={() => {
           Alert.alert(
            title,
            message,
            [
              {text: '不带伞', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
              {text: '知道了', onPress: () => console.log('OK Pressed')},
            ],
          )
         }}/>
         <Button title="标题+内容+多按钮" onPress={() => {
           Alert.alert(
            title,
            message,
            [
              {text: '不带伞', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
              {text: '知道了', onPress: () => console.log('OK Pressed')},
              {text: '有人送伞', onPress: () => console.log('送伞')},
            ],
            {
              cancelable: true,
              onDismiss: () => {
                ToastAndroid.show('点击了外面', ToastAndroid.SHORT)
              }
            }
          )
         }}/>
         <Button title="iOS" onPress={() => {
           AlertIOS.alert(
             title,
             message,
             [
               {text: '取消',onPress: function() {console.log('取消按钮点击');}},
               {text: '确认',onPress: function() {console.log('确认按钮点击');}},
             ]
           )
         }}/>
         <Button title="iOS+输入框" onPress={() => {
           AlertIOS.prompt(
             title,
             message,
             [
               {text: '取消',onPress: function() {console.log('取消按钮点击');}},
               {text: '确认',onPress: (text) => {
                 _this.setState({inputText:text})
                 console.log('你输入了: ' + _this.state.inputText)
               }},
             ],
             'secure-text',
             this.state.inputText,
             'number-pad'
           )
         }}/>

)

       </View>
     )
   }
}
