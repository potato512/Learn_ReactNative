import React, { Component } from 'react';
import {
    StyleSheet,
    View,
    Text,
    Alert
} from 'react-native';

type Props = {};
export default class TextPage extends Component<Props> {
    render() {
        return(
            <View style={styles.containerStyle}>
                <Text style={styles.textStyle}>文字组件</Text>
                <Text style={{margin:10, fontWeight:"normal", fontSize:20, color:"green", textAlign:'right', writingDirection:'rtl'}}>
                    左对齐且从右向左排列显示。
                </Text>
                <Text style={{margin:10, backgroundColor:'#E6E6FA', color:"brown"}}>
                    该组件为React中的一个基本组件,和Android的TextView组件类似,用来显示基本的文本信息,除了基本的显示布局之外,也可以进行嵌套布局
                    ,设置样式,以及做事件处理.
                </Text>
                <Text style={{margin:10, backgroundColor:'#E6E6FA', color:"brown"}} numberOfLines={3}>
                    该组件为React中的一个基本组件,和Android的TextView组件类似,用来显示基本的文本信息,除了基本的显示布局之外,也可以进行嵌套布局
                    ,设置样式,以及做事件处理.
                </Text>
                <Text style={{height:30,margin:10}} onPress={() => {
                    Alert.alert("单击了文字组件");
                }}>文字单击</Text>
                <Text style={{height:30,margin:10}} onLongPress={() => {
                    Alert.alert("长按了文字组件")
                }}>文字长按</Text>
            </View>

        );
    }
}

var styles = StyleSheet.create({
    containerStyle: {
        margin:20,
        backgroundColor:'#FFFACD',
    },
    textStyle: {
        height:50,
        backgroundColor:"#DCDCDC",

        color:"#F08080",

        fontSize: 30,
        fontStyle:'normal',
        fontWeight:'bold',

        // 'none', 'underline', 'line-through', 'underline line-through'
        textDecorationLine: 'underline',
        // 'solid', 'double', 'dotted', 'dashed'
        textDecorationStyle: 'dashed',
        textDecorationColor: 'black',

        letterSpacing:10,
        lineHeight:50,
        writingDirection:'auto', // auto,ltr,rtl
        textAlign:'center',

        // 文字阴影偏移
        textShadowOffset: {width: 10, height: 10},
        // 文字阴影颜色
        textShadowColor: 'black',
        // 文字阴影圆角的大小
        textShadowRadius: 5,
    }
});