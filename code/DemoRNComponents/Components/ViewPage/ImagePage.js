import React, {Component} from 'react';
import {
    StyleSheet,
    View,
    Image,
    Text
} from 'react-native';

const imgUrl = {
    uri:"https://f10.baidu.com/it/u=1934388360,3660384600&fm=72"
};

type Props = {};
export default class ImagePage extends Component<Props> {
    render() {
        return(
          <View style={styles.containerStyle}>
              <View style={{flexDirection:'row', flexWrap:"wrap", justifyContent:"space-around"}}>
                  <Image style={styles.imageUrlStyle} source={imgUrl}></Image>

                  <View style={{justifyContent:"center", alignItems:"center"}}>
                      <Image style={[styles.imageUrlStyle, {resizeMode:'contain'}]}
                             source={{uri:'https://f10.baidu.com/it/u=1934388360,3660384600&fm=72'}} />
                      <Text style={{position:"absolute", top:20, left:20, color:"#FF4500",}}>
                          contain全显缩略模式
                      </Text>
                  </View>

                  <Image style={[styles.imageUrlStyle, {resizeMode:'center'}]} source={imgUrl}></Image>
                  <Image style={[styles.imageUrlStyle, {resizeMode:'stretch'}]} source={imgUrl}></Image>
              </View>

            <Image style={styles.imageLocalStyle} source={require("../../Resources/01.png")}></Image>
          </View>
        );
    }
};

const styles = StyleSheet.create({
  containerStyle: {
      marginTop:20,
      justifyContent:'center',
      alignItems:'center',
  },
  imageUrlStyle: {
      margin:10,
      width: 150,
      height: 150,
      backgroundColor: '#C0C0C0',

      // 显示模式：缩略全显contain，拉升全显（会变形）stretch，裁剪后显示（默认）cover
      resizeMode:'cover',
  },
  imageLocalStyle: {
      margin:10,
      width: 120,
      height: 120,
      backgroundColor: '#C0C0C0',

      // 显示模式：缩略全显contain，拉升全显（会变形）stretch，裁剪后显示（默认）cover
      resizeMode:'contain',
  }
});