import React, { Component } from 'react';
import {
    StyleSheet,
    View,
    Text,
    Alert,
    Keyboard,
    TextInput,
    TouchableWithoutFeedback,
    TouchableHighlight
} from 'react-native';

type Props = {};
export default class TextInputPage extends Component<Props> {

    // constructor(props) {
    //     super(props);
    //     this.state = {
    //         inputText:'',
    //         isEditing:true,
    //         heightKeyboard:0
    //     };
    // }
    // 或
    state = {
        inputText:'',
        isEditing:true,
        heightKeyboard:0
    };

    render() {

        return(
            <TouchableWithoutFeedback onPress={() => {
              Keyboard.dismiss()
            }}>
            <View style={{margin:20,backgroundColor:"#F8F8FF"}}>
                <Text style={{margin:10,color:"red",backgroundColor:'yellow'}}>
                    TextInput是一个允许用户在应用中通过键盘输入文本的基本组件。本组件的属性提供了多种特性的配置，譬如自动完成、自动大小写、占位文字，以及多种不同的键盘类型（如纯数字键盘）等等。
                </Text>
                <TextInput style={styles.inputStyle}
                           // 是否多行
                           multiline={true}
                           // 输入框行数（无效？？）
                           numberOfLines={4}
                           // 是否自动修正拼写
                           autoCorrect={false}
                           // characters: 所有的字符 words: 每个单词的第一个字符 sentences: 每句话的第一个字符（默认） none: 不自动切换任何字符为大写。
                           autoCapitalize={'characters'}
                           // 是否可编辑
                           editable={true}
                           // 键盘类型 "default", 'numeric', 'email-address', "ascii-capable", 'numbers-and-punctuation', 'url', 'number-pad', 'phone-pad', 'name-phone-pad', 'decimal-pad', 'twitter', 'web-search'
                           keyboardType={'default'}
                           // 键盘确定按钮类型 'done', 'go', 'next', 'search', 'send', 'none', 'previous', 'default', 'emergency-call', 'google', 'join', 'route', 'yahoo'
                           returnKeyType={'done'}
                           // 键盘颜色 'default', 'light', 'dark'
                           keyboardAppearance={'dark'}
                           // 可输入字符数量
                           maxLength={2000}
                           // 占位符
                           placeholder={'请输入反馈内容'}
                           placeholderTextColor={'blue'}
                           // 默认内容
                           // defaultValue={'input组件'}
                            // 是否密码输入（android有效，ios无效）
                           secureTextEntry={true}
                           // 是否要在文本框右侧显示“清除”按钮 'never', 'while-editing', 'unless-editing', 'always'
                           clearButtonMode={"while-editing"}
                           // 自动获取焦点
                           autoFocus={this.state.isEditing}

                           // 交互
                           // 文字编辑改变时的方法
                           onChangeText={(text) => this.setState({inputText:text})}
                           // 编辑焦点消失时的方法
                           onBlur={(event) => {
                               console.log("失去焦点：" + event)
                           }}
                           // 编辑焦点获取时的方法
                           onFocus={(event) => {
                               console.log("获取焦点：" + event)
                           }}
                           // 编辑结束时的方法
                           onEndEditing={(event) => {
                               console.log("结束编辑：" + event)
                           }}

                >

                </TextInput>
                <Text style={{margin:10}}>已输入{this.state.inputText.length}个字。</Text>
                <TouchableHighlight
                    style={{margin:10,width:60,height:30,backgroundColor:"#E6E6FA",alignItems:"center",justifyContent:"center"}}
                    onPress={() => {
                      Keyboard.dismiss();

                        // this.setState({isEditing:false});
                        // let message = "键盘高：" + this.state.heightKeyboard + "\n输入内容：" + this.state.inputText;
                        // Alert.alert(message);
                    }}
                >
                    <Text>完成</Text>
                </TouchableHighlight>
            </View>
          </TouchableWithoutFeedback>
        );
    }
}


var styles = StyleSheet.create({
    inputStyle: {
        margin:10,
        height:100,

        backgroundColor:'#D8BFD8',
    }
});
