import React, { Component } from 'react';
import {
    StyleSheet,
    View,
    FlatList,
    Text
} from 'react-native';

type Props = {};
export default class FlastListPage extends Component<Props> {
    render() {
        return(
            <View>
                <FlatList data={[{key:'a'}, {key:'b'}, {key:'c'}]}
                          renderItem={({item}) => <Text style={styles.itemStyle}>{item.key}</Text>} />
            </View>
        );
    }
};

const styles = StyleSheet.create({
    itemStyle: {
        height: 40,

        paddingHorizontal: 20,
        backgroundColor:"#F5F5F5",
    }
})