import React, { Component } from 'react';
import {
    StyleSheet,
    View,
    Text,
    SectionList
} from 'react-native';

type Props = {};
export default class SectionListPage extends Component<Props> {

    // 定义分组中的每个单元格
    listRow = (info) => {
        var txt = '  ' + info.item.type;
        return <Text style={styles.sectionItem}>{txt}</Text>
    };

    // 定义分组
    listSection = (info) => {
        var txt = info.section.key;
        return <View style={styles.sectionHeader}><Text style={styles.sectionTxtHeader}>{txt}</Text></View>
    };

    render() {

        // 定义数据源
        let sections = [
            { key: "UI View", data: [{ type: "View" }, { type: "Text" }, { type: "Image" }, {type: "TextInput"}, {type: "StyleSheet"}, {type: "ScrollView"}] },
            { key: "UI Other", data: [{ type: "ActivityIndicator" }, { type: "Alert" }, { type: "Animated" }, { type: "CameraRoll" }, { type: "Clipboard" }, {type:"Demensions"}, {type:"KeyboardAvoiding"}, {type:"Linking"}, {type:"Modal"}, {type:"PixedRation"}, {type:"RefreshControl"}, {type:"StatusBar"}, {type:"WebView"}] },
            { key: "UI List", data: [{ type: "FlatList" }, { type: "SectionList" }] },
            { key: "UI iOS", data: [{ type: "ActionSheet" }, { type: "AlertIOS" }, { type: "DatePickerIOS" },{ type: "ImagePickerIOS" }, { type: "NavigatorIOS" }, { type: "ProgressView" }, {type:"PushNotificationIOS"}, {type:"SegmentControlIOS"}, {type:"TabBarIOS"}] },
            { key: "UI Android", data: [{ type: "BackHandleAndroid" }, { type: "DatePickerAndroid" }, {type:"DrawLayoutAndroid"}, {type:"PermissionsAndroid"}, {type:"ProgressBarAndroid"}, {type:"TimePickerAndroid"}, {type:"ToastAndroid"}, {type:"ToolbarAndroid"}, {type:"ViewAndroid"}] },
            { key: "UI userInterface", data: [{ type: "Button" }, { type: "Picker" }, {type: "Slider"}, {type:"Switch"}] },
        ];

        return(
            <View>
                /* 分组列表*/
                <SectionList
                    renderSectionHeader={this.listSection}
                    renderItem={this.listRow}
                    ItemSeparatorComponent={() => <View style={{height:0.5, backgroundColor:"#FF0000"}}></View>}
                    ListHeaderComponent={HeaderComponent}
                    ListFooterComponent={FooterComponent}
                    sections={sections}>
                </SectionList>

            </View>
        )
    }
}

// 定义分组列表表头
const HeaderComponent = (() => <View style={styles.headerStyle}><Text style={styles.headerTxtStyle}>组件学习</Text></View>);
// 定义分组列表表尾
const FooterComponent = (() => <View style={styles.footerStyle}><Text style={styles.footerTxtStyle}>组件学习</Text></View>);

// 样式定义
const styles = StyleSheet.create({
    headerStyle: {
        height: 50,
        paddingHorizontal:20,
        backgroundColor:"#E6E6FA"
    },
    headerTxtStyle: {
        lineHeight: 50,
        textAlign:"left",
        fontSize:30,
        color:"#8A2BE2"
    },
    footerStyle: {
        height: 50,
        paddingHorizontal:20,
        backgroundColor:"#000000"
    },
    footerTxtStyle: {
        lineHeight:50,
        textAlign:"right",
        fontSize:20,
        color:"#EE82EE"
    },
    sectionHeader: {
        height:40,

        paddingHorizontal:10,
        justifyContent:"center",
        // alignItems:"center",
    },
    sectionTxtHeader: {
        fontSize:26,
    },
    sectionItem: {
        height:30,

        textAlign: "center",
        color: "#FFFFFF",
        fontSize:24,

        backgroundColor: '#9CEBBC'
    }
});