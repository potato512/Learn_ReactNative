import React, { Component } from 'react';
import {
    StyleSheet,
    View,
    Text,
    TouchableOpacity,
    Alert
} from 'react-native';

type Props = {};
export default class TouchableOpacityPage extends Component<Props> {
    render() {
        return(
            <View style={styles.viewContainer}>
                <Text style={styles.textStyle} onPress={() => {
                    Alert.alert("点击了Text");
                }}>
                    TouchableOpacity组件用于封装视图，使其可以正确响应触摸操作。当按下的时候，封装的视图的不透明度会降低。
                </Text>
                <TouchableOpacity onPress={() => {
                    Alert.alert("点击弹窗");
                }}>
                    <Text style={{margin:10, width:80, height:30, lineHeight:30, backgroundColor:"#F0F8FF", textAlign:'center'}}>
                        点击
                    </Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={searchClick}>
                    <Text style={{margin:10, width:50, height:30, lineHeight:30, backgroundColor:"#1E90FF", textAlign:'center'}}>
                        搜索
                    </Text>
                </TouchableOpacity>
            </View>
        );
    }
}

const searchClick = () => {
    Alert.alert("点击搜索");
};

const styles = StyleSheet.create({
    viewContainer: {
        margin:20,
        backgroundColor:"#D8BFD8",
    },
    textStyle: {
        margin:10,
        color:'red',
    }
});