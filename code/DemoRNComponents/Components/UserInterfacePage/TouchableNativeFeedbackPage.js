import React, { Component } from 'react';
import {
    StyleSheet,
    View,
    Text,
    Alert,
    TouchableNativeFeedback
} from 'react-native';
import TouchableHighlightPage from "./TouchableHighlightPage";

type Props = {};
export default class TouchableNativeFeedbackPage extends Component<Props> {
    render() {
        return(
            <View style={styles.viewContainer}>
                <Text style={styles.textStyle}>
                    TouchableNativeFeedbackPage组件用于封装视图，使其可以正确响应触摸操作（仅限Android平台）。在Android设备上，这个组件利用原生状态来渲染触摸的反馈。
                </Text>
                <TouchableNativeFeedback onPress={() => {
                    Alert.alert("确定按钮")
                }}>
                    <Text style={styles.touchStyle}>确定</Text>
                </TouchableNativeFeedback>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    viewContainer:{
        margin:20,
        backgroundColor:"#FFF8DC",
    },
    textStyle:{
        margin:10,
        backgroundColor:"#FFF5EE",
        color:"#FF6347",
    },
    touchStyle:{
        margin:10,
        backgroundColor:"#6495ED",
        width:60,
        height:30,
        textAlign:"center",
        lineHeight:30,
    }
});