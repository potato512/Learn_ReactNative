import React, {Component} from 'react';
import {
    StyleSheet,
    View,
    Button,
    Alert
} from 'react-native';

type Props = {};
export default class ButtonPage extends Component<Props> {
    render() {
        return(
            <View style={{marginTop:20,marginHorizontal:10}}>

                <View style={styles.clickButtonStyle}>
                    <Button title={"点我"} color={'green'} onPress={buttonClick}/>
                </View>


                <Button title={"单击"} onPress={() => {
                    Alert.alert("单击按钮");
                }} />

            </View>
        );
    };
}

const buttonClick = () => {
    Alert.alert("点击了按钮");
};

const styles = StyleSheet.create({
   clickButtonStyle: {
       margin: 10,

       height: 40,

       backgroundColor: '#E6E6FA',

       borderRadius: 10,
       borderWidth: 1,
       borderColor: '#7FFF00',
   },
});