import React, { Component } from 'react';
import {
    StyleSheet,
    View,
    Text,
    Alert,
    TouchableHighlight
} from 'react-native';

type Props = {};
export default class TouchableHighlightPage extends Component<Props> {
    render() {
        return(
            <View style={styles.viewContainer}>
                <Text style={styles.textStyle}>
                    TouchableHighlight组件用于封装视图，使其可以正确响应触摸操作。当按下的时候，封装的视图的不透明度会降低，同时会有一个底层的颜色透过而被用户看到，使得视图变暗或变亮。
                </Text>
                <TouchableHighlight style={styles.touchStyle} underlayColor={"#FFFF00"} onPress={() => {
                    Alert.alert("查找")
                }}>
                    <Text>查找</Text>
                </TouchableHighlight>
                <TouchableHighlight style={[styles.touchStyle, {width:120, backgroundColor:"#fffff0"}]} onLongPress={() => {
                    Alert.alert("长按——确定")
                }}>
                    <Text>长按——确定</Text>
                </TouchableHighlight>
            </View>
        );
    }
}

const styles = StyleSheet.create({
   viewContainer:{
       margin:20,
       backgroundColor:"#F0FFFF",
   },
   textStyle:{
       margin:10,
       backgroundColor:"#FFFFF0",
       color:"blue",
   },
   touchStyle:{
       margin:10,
       width:60,
       height:30,
       backgroundColor:"brown",

       justifyContent:"center",
       alignItems:"center",
   }
});