import { AppRegistry } from 'react-native';
import App from './App';

// 4 注册程序入口组件，即注册根容器
AppRegistry.registerComponent('DemoRNComponents', () => App);
