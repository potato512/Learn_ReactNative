# Learn_ReactNative
react native跨平台开发

学习交流`https://gitee.com/potato512/Learn_ReactNative`

react-native学习交流QQ群`806870562`

### 学习网站
* [React Native中文网](https://reactnative.cn)
* [ECMAScript 6 入门](http://es6.ruanyifeng.com)
* [react-navigation](https://reactnavigation.org/)

### 开发环境

### 开发工具
* [Atom](https://atom.io/)
  * [Atom插件](https://atom.io/packages)
  * 其他插件：
    * 浏览器浏览功能`open-in-broser`
    * 分页展示html页面效果`atom-html-preview`
    * 文件路径补全`autocomplete-path`
    * 文档化注释`docblockr`
    * js片段代码自动补全`javascript-snippets`
    * 大名鼎鼎的Zen coding，可以快速生成代码，提高编码速度`Emmet`
    * React Native开发时的插件`nuclide`（安装方法：打开Atom-Settings-Insatll Packages-搜索中输入nuclide-Packages-Install-安装成功后-Settings-勾选Install Recommended Packages on Startup）
    * css前缀自动补全`autoprefixer`
    
> 注意：插件安装方法：Atom——>菜单栏——>preferences——>settings——>Install——>搜索（packages）——>Install。


### 项目结构

### 项目创建

### 组件学习
* 基础组件
  * View
  * Text——20180515
  * Image——20180511
  * TextInput——20180614
  * ScrollView——20180522
  * StyleSheet——20180515
* 交互组件
  * Button——20180511
  * TouchableHighlight——20180517
  * TouchableOpacity——20180517
  * TouchableNativeFeedback——20180518
  * Picker
  * Slider
  * Switch
* 列表组件
  * Flatlist
  * SectionList——20180514
* 其他组件
  * ActivityIndicator
  * Alert——20180621
  * Animated
  * CameraRoll
  * Clipboard
  * Dimensions
  * KeyboardAvoidingView
  * Linking
  * Modal
  * PixelRadio
  * RefreshControl
  * StatusBar
  * WebView——20180614
* iOS组件
  * ActionSheetIOS
  * AlertIOS——20180621
  * DatePickerIOS
  * ImagePickerIOS
  * NavigatorIOS
  * ProgressViewIOS
  * PushNotificationIOS
  * SegmentedControlIOS
  * TabBarIOS
* Android组件
  * BackHandler
  * DatePickerAndroid
  * DrawerLayoutAndroid
  * PermissionsAndroid
  * ProgressBarAndroid
  * TimePickerAndroid
  * ToastAndroid
  * ToolbarAndroid
  * ViewPagerAndroid
  
### 页面处理
  * 页面导航处理——20180621
  * 页签切换处理
  * 页页抽屉式处理
  
### 多媒体
  * 图片相册
  * 拍照
  * 视频
  * 音频

### 缓存学习——20180614
* AsyncStorage保存
* AsyncStorage读取
* AsyncStorage修改
* AsyncStorage删除
  * AsyncStorage删除指定的
  * AsyncStorage删除全部

### 网络学习
* fetch网络请求——20180621

### 交互学习
* 手势
  * 单击/双击
  * 长按
  * 双击
  * 拖放
  * 旋转
  * 拿捏
* 点击

### 其他
* 消息
* 定时器
* 动画
* 生命周期——20180621

